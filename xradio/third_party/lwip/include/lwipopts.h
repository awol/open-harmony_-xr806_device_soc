/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _XRADIO_OHOS_LWIPOPTS_H_
#define _XRADIO_OHOS_LWIPOPTS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include_next "lwipopts.h"

/*
   --------------------------------------------------
   ---------- New options added for XRadio ----------
   --------------------------------------------------
*/

#define LWIP_XR_IMPL 1 // XRadio's implementation
#define LWIP_XR_MEM 0 // XRadio's implementation of memory
#define LWIP_XR_DEINIT 0 // lwIP deinit
#define LWIP_BUG_FIXED 1 // Bug fixed for lwIP
#define LWIP_SUPPRESS_WARNING 0
#define LWIP_RESOURCE_TRACE 0 // trace resource usage for debugging
#define LWIP_MBOX_TRACE 0 // trace mbox usage for debugging

#undef MEM_SIZE
#define MEM_SIZE (24 * 1024)

#undef MEMP_NUM_PBUF
#define MEMP_NUM_PBUF 6

#undef MEMP_NUM_RAW_PCB
#define MEMP_NUM_RAW_PCB 2

#undef MEMP_NUM_UDP_PCB
#define MEMP_NUM_UDP_PCB 10

#undef MEMP_NUM_TCP_PCB
#define MEMP_NUM_TCP_PCB 8

#undef MEMP_NUM_TCP_PCB_LISTEN
#define MEMP_NUM_TCP_PCB_LISTEN 2

#undef MEMP_NUM_TCP_SEG
#define MEMP_NUM_TCP_SEG 24

#undef MEMP_NUM_ALTCP_PCB
#define MEMP_NUM_ALTCP_PCB MEMP_NUM_TCP_PCB

#undef MEMP_NUM_REASSDATA
#define MEMP_NUM_REASSDATA 2

#undef MEMP_NUM_FRAG_PBUF
#define MEMP_NUM_FRAG_PBUF 2

#undef MEMP_NUM_ARP_QUEUE
#define MEMP_NUM_ARP_QUEUE 16

#undef MEMP_NUM_IGMP_GROUP
#define MEMP_NUM_IGMP_GROUP 3

#undef MEMP_NUM_NETBUF
#define MEMP_NUM_NETBUF 10

#undef MEMP_NUM_SELECT_CB
#define MEMP_NUM_SELECT_CB 4

#undef MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_TCPIP_MSG_API 4
#define MEMP_NUM_API_MSG                MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_DNS_API_MSG            MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_SOCKET_SETGETSOCKOPT_DATA MEMP_NUM_TCPIP_MSG_API
#define MEMP_NUM_NETIFAPI_MSG           MEMP_NUM_TCPIP_MSG_API

#undef MEMP_NUM_TCPIP_MSG_INPKT
#define MEMP_NUM_TCPIP_MSG_INPKT 16

#undef MEMP_NUM_NETDB
#define MEMP_NUM_NETDB 1

#undef MEMP_NUM_LOCALHOSTLIST
#define MEMP_NUM_LOCALHOSTLIST 1

#undef PBUF_POOL_SIZE
#define PBUF_POOL_SIZE 14


#undef TCP_WND
#define TCP_WND (6 * TCP_MSS)

#undef TCP_MAXRTX
#define TCP_MAXRTX 12
#define TCP_SYNMAXRTX 6

#undef TCP_MSS
#define TCP_MSS 1460

#undef TCP_SND_BUF
#define TCP_SND_BUF (6 * TCP_MSS)

#undef TCP_SND_QUEUELEN
#define TCP_SND_QUEUELEN                                                       \
    LWIP_MIN(                                                              \
        MEMP_NUM_TCP_SEG,                                              \
        ((4 * (TCP_SND_BUF) + (TCP_MSS - 1)) /                         \
         (TCP_MSS))) //((4 * (TCP_SND_BUF) + (TCP_MSS - 1))/(TCP_MSS))

#undef PBUF_POOL_BUFSIZE
#define PBUF_POOL_BUFSIZE                                                      \
    LWIP_MEM_ALIGN_SIZE(TCP_MSS + 40 + PBUF_LINK_ENCAPSULATION_HLEN +      \
                PBUF_LINK_HLEN)

#define TCPIP_THREAD_NAME              "tcpip"

#undef TCPIP_THREAD_STACKSIZE
#define TCPIP_THREAD_STACKSIZE          (2 * 1024)

#undef TCPIP_THREAD_PRIO
#define TCPIP_THREAD_PRIO               (3)

#undef TCPIP_MBOX_SIZE
#define TCPIP_MBOX_SIZE                 32

#define LWIP_TCPIP_THREAD_ALIVE()
#define SLIPIF_THREAD_NAME             "slipif_loop"

#define DEFAULT_THREAD_NAME            "lwIP"

#define DEFAULT_THREAD_STACKSIZE        (1024)

#define DEFAULT_THREAD_PRIO             (TCPIP_THREAD_PRIO)

#undef DEFAULT_RAW_RECVMBOX_SIZE
#define DEFAULT_RAW_RECVMBOX_SIZE       4

#undef DEFAULT_UDP_RECVMBOX_SIZE
#define DEFAULT_UDP_RECVMBOX_SIZE       8

#undef DEFAULT_TCP_RECVMBOX_SIZE
#define DEFAULT_TCP_RECVMBOX_SIZE       8

#undef DEFAULT_ACCEPTMBOX_SIZE
#define DEFAULT_ACCEPTMBOX_SIZE         4

#undef RECV_BUFSIZE_DEFAULT 
#define RECV_BUFSIZE_DEFAULT            (10 * 1024)

/* ARP function */
#define LWIP_ARP 1
#define ARP_TABLE_SIZE 8
#define ARP_MAXAGE 300

#undef IP_REASS_MAX_PBUFS
#define IP_REASS_MAX_PBUFS 7

#undef LWIP_DHCP_AUTOIP_COOP_TRIES
#define LWIP_DHCP_AUTOIP_COOP_TRIES 5

#undef LWIP_CONFIG_NUM_SOCKETS
#define LWIP_CONFIG_NUM_SOCKETS 20

#undef IP_FRAG_MAX_MTU
#define IP_FRAG_MAX_MTU 150

#define LWIP_ALLOW_MEM_FREE_FROM_OTHER_CONTEXT 1

#define LWIP_SOCKET_SELECT_FUNC
#define LWIP_NETIF_STATUS_CALLBACK      1

#define DHCP_DOES_ARP_CHECK             ((LWIP_DHCP) && (LWIP_ARP))

#ifdef __cplusplus
}
#endif

#endif /* _NET_ETHERNETIF_ETHERNETIF_H_ */
