# HDF移植简介

## HDF的作用

HDF对于OpenHarmony轻量设备来说，主要意义是统一接口。不同于过去OpenHarmony的util接口，过去的接口虽然是统一的，但是输入的数据结构是写死的，适配厂家必须根据util_API预留的数据结构，想办法把自身的数据结构，转换为util的数据结构，经常出现结构体的元素缺失、溢出、类型不符等情况，并不能把芯片原有的能力很好的发挥出来。

## HDF适配

[官方说明文档](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/driver/driver-hdf.md#/openharmony/docs/blob/master/zh-cn/device-dev/driver/driver-hdf-development.md)。

以GPIO的适配为例进行说明。

指定HDF路径的方式是无约束的，因为hcs的配置是依赖于硬件，也就是依赖于开发板的，所以对于XRADIO，HDF的编译配置是这样子的：

device/board/AWOL/drivers/BUILD.gn：

```makefile
import("//drivers/adapter/khdf/liteos_m/hdf.gni")			#模板
module_name = get_path_info(rebase_path("."), "name")

hdf_driver(module_name) {
  hcs_sources = ["${module_name}.hcs"]						#调用hcs文件
  visibility += [
    "$device_path",
    ".",
  ]
}
```

device/board/AWOL/drivers/device_info.hcs的模板是相对固定的：

```c
root {
    module = "xradio,xr806";
    device_info {								//类似于C++的类
        match_attr = "hdf_manager";
        template host {							//创建host模板
            hostName = "";
            priority = 100;
            template device {
                template deviceNode {
                    policy = 0;
                    priority = 100;
                    preload = 0;
                    permission = 0664;
                    moduleName = "";
                    serviceName = "";
                    deviceMatchAttr = "";
                }
            }
        }
        platform :: host {					  //继承host的模板
            hostName = "platform_host";
            priority = 50;
            device_gpio :: device {
                gpio0 :: deviceNode {
                    policy = 2;					//对模板的值进行修改，如果不修改则套用默认值
                    priority = 100;
                    moduleName = "XRADIO_GPIO";
                    serviceName = "HDF_PLATFORM_GPIO";
                    deviceMatchAttr = "gpio_config";
                }
            }
        }
    }
}
```

device/board/AWOL/drivers/drivers.hcs的内容有用户随意编写

```c
#include "device_info.hcs"
root {
    platform {
        gpio_config {
            match_attr = "gpio_config";
            /*
                格式说明：
                pinport : 0portA，1portB
                pinnum : 根据实际选择
                driving ：驱动能力范围 [0,1,2,3]
                mode ：0输入，1输出，6中断，7禁止使用
                pull : 0悬空，1上拉，2下拉
            */
           pinport = 0;								//内容由开发者随意填，在C代码中都能获取
           pinnum = 21;
           driving = 1;
           mode = 1;
           pull = 0;
        }
    }
}

```

### 接口代码的适配

device/soc/AWOL/xradio/drivers/gpio/gpio_xradio.c：

```C
struct HdfDriverEntry g_GpioDriverEntry = {
    .moduleVersion = 1,
    .moduleName = "XRADIO_GPIO",				//必须与device_info.hcs中保持一致
    .Bind = GpioDriverBind,						//Bind代表获取controler
    .Init = GpioDriverInit,						//初始化函数，名称可以自定义
    .Release = GpioDriverRelease,				//去初始化函数，名称可以自定义
};
HDF_INIT(g_GpioDriverEntry);					//需要在ld中固定，否则会被丢弃
```

获取在hcs中自定义的信息

```C
struct GpioMethod g_GpioCntlrMethod = {
    .request = NULL,
    .release = NULL,
    .write = GpioDevWrite,
    .read = GpioDevRead,
    .setDir = GpioDevSetDir,
    .getDir = GpioDevGetDir,
    .toIrq = NULL,
    .setIrq = GpioDevSetIrq,
    .unsetIrq = GpioDevUnSetIrq,
    .enableIrq = GpioDevEnableIrq,
    .disableIrq = GpioDevDisableIrq,
};

static int32_t AttachGpioDevice(struct GpioCntlr *gpioCntlr,
                                struct HdfDeviceObject *device)
{
    int32_t pinport, pinnum;
    GPIO_InitParam param;
    struct GpioDevice *gpioDevice = NULL;
    if (device->property == NULL) {
        HDF_LOGD("%s: property is NULL\n", __func__);
        return HDF_FAILURE;
    }

    struct DeviceResourceIface *dri =
            DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (dri == NULL || dri->GetUint32 == NULL) {
        HDF_LOGD("DeviceResourceIface is invalid\n");
        return HDF_FAILURE;
    }
    dri->GetUint32(device->property, "pinport", &pinport, 0);
    dri->GetUint32(device->property, "pinnum", &pinnum, 0);
    dri->GetUint32(device->property, "driving", &param.driving, 0);
    dri->GetUint32(device->property, "mode", &param.mode, 0);
    dri->GetUint32(device->property, "pull", &param.pull, 0);
    HAL_GPIO_Init(pinport, pinnum, &param);

    gpioCntlr->start = (pinport * (GPIOA_PIN_MAX + 1)) + pinnum;

    return HDF_SUCCESS;
}

static int32_t GpioDriverInit(struct HdfDeviceObject *device)
{
    int32_t ret;
    struct GpioCntlr *gpioCntlr = NULL;

    HDF_LOGD("GPIO Init!\n", __func__);
    if (device == NULL) {
        HDF_LOGD("%s: device is NULL\n", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }
    if (device->service == NULL) {
        HDF_LOGD("%s: service is NULL\n", __func__);
    }
    //查询device是否为空
    gpioCntlr = GpioCntlrFromDevice(device);
    if (gpioCntlr == NULL) {
        HDF_LOGD("GpioCntlrFromDevice fail\n");
        return HDF_FAILURE;
    }
    //获取HCS预设的信息
    ret = AttachGpioDevice(gpioCntlr,
                           device); // GpioCntlr add GpioDevice to priv
    if (ret != HDF_SUCCESS) {
        HDF_LOGD("AttachGpioDevice fail\n");
        return HDF_FAILURE;
    }
    //初始化计数+1
    gpioCntlr->count ++;
    gpioCntlr->ops = &g_GpioCntlrMethod; // register callback
    ret = GpioCntlrAdd(gpioCntlr);

    if (ret != HDF_SUCCESS) {
        HDF_LOGD("ret = %d\r\n", ret);
        HDF_LOGD("GpioCntlrAdd fail %d\r\n", gpioCntlr->start);
        return HDF_FAILURE;
    }
    return HDF_SUCCESS;
}
```

### 编译

执行编译时，OpenHarmony会自动把.hcs文件翻译成.hex文件，生成到out/xradio/xradio_wifi_demo/gen/device/board/AWOL/drivers/drivers_hex.c，把.hcs的代码翻译成一个大数组，供hdf调用。
