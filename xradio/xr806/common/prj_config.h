/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _PRJ_CONFIG_H_
#define _PRJ_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

/*
 * project base config
 */

/* main thread priority */
#define PRJCONF_MAIN_THREAD_PRIO        OS_THREAD_PRIO_APP

/* main thread stack size */
#define PRJCONF_MAIN_THREAD_STACK_SIZE  (2 * 1024)

/* sys ctrl enable/disable */
#define PRJCONF_SYS_CTRL_EN             1

/* sys ctrl thread priority */
#define PRJCONF_SYS_CTRL_PRIO           OS_THREAD_PRIO_SYS_CTRL

/* sys ctrl stack size */
#define PRJCONF_SYS_CTRL_STACK_SIZE     (2 * 1024)

/* sys ctrl queue length for receiving message */
#define PRJCONF_SYS_CTRL_QUEUE_LEN      6

/* image flash ID */
#define PRJCONF_IMG_FLASH               0

/* image start address, including bootloader */
#define PRJCONF_IMG_ADDR                0x00000000

/* save sysinfo to flash or not */
#define PRJCONF_SYSINFO_SAVE_TO_FLASH   1

#if PRJCONF_SYSINFO_SAVE_TO_FLASH

/* sysinfo flash ID */
#define PRJCONF_SYSINFO_FLASH           0

/* sysinfo start address */
#define PRJCONF_SYSINFO_ADDR            ((1536 - 4) * 1024)

/* sysinfo size */
#define PRJCONF_SYSINFO_SIZE            (4 * 1024)

/* enable/disable checking whether sysinfo is overlap with image */
#define PRJCONF_SYSINFO_CHECK_OVERLAP   1

#endif /* PRJCONF_SYSINFO_SAVE_TO_FLASH */

/* MAC address source */
#define PRJCONF_MAC_ADDR_SOURCE         SYSINFO_MAC_ADDR_CHIPID

/* watchdog enable/disable */
#define PRJCONF_WDG_EN                  1

/* watchdog timeout value */
#define PRJCONF_WDG_TIMEOUT             WDG_TIMEOUT_16SEC

/* watchdog feeding period (in ms), MUST less than PRJCONF_WDG_TIMEOUT */
#define PRJCONF_WDG_FEED_PERIOD         (10 * 1000)

/*
 * project hardware feature
 */

/* uart enable/disable */
#define PRJCONF_UART_EN                 1

/* h/w crypto engine enable/disable */
#define PRJCONF_CE_EN                   1

/* spi enable/disable */
#define PRJCONF_SPI_EN                  1

/* Xradio internal codec sound card enable/disable */
#define PRJCONF_INTERNAL_SOUNDCARD_EN   1

/* AC107 sound card enable/disable */
#define PRJCONF_AC107_SOUNDCARD_EN      0

/* AC101 sound card enable/disable */
#define PRJCONF_AC101_SOUNDCARD_EN      0

/* I2S NULL sound card enable/disable */
#define PRJCONF_I2S_NULL_SOUNDCARD_EN   0

#define PRJCONF_SWD_EN 1

/*
 * project service feature
 */

/* console enable/disable */
#define PRJCONF_CONSOLE_EN              1

/* network and wlan enable/disable */
#define PRJCONF_NET_EN                  1

#ifdef __cplusplus
}
#endif

#endif /* _PRJ_CONFIG_H_ */
