# XRADIO

## 目录介绍

```shell
xradio
├── Kconfig.liteos_m.defconfig.series					# xr系列默认宏配置
├── Kconfig.liteos_m.series								# 同系列芯片配置
├── Kconfig.liteos_m.soc								# 芯片SOC配置
├── adapter												# ohos接口适配
├── osal												# rtos接口适配
├── third_party											# 三方库接口适配
└── xr806												# xr806代码仓
    ├── Kconfig.liteos_m.defconfig.xradio				# xr806默认宏配置
    ├── create-image.py									# img文件编译脚本
    ├── BUILD.gn										# gn编译脚本
    ├── build											# 编译构建文件夹
    ├── include											# 芯片层头文件
    ├── src												# 芯片层源码
    ├── lib												# 静态库
    ├── project											# 系统框架代码/应用代码
    └── tools											# 辅助工具
```

## 编译设置

1. 下载编译工具链[gcc-arm-none-eabi-10-2020-q4-major](https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2?revision=ca0cbf9c-9de2-491c-ac48-898b5bbc0443&hash=3710A129B3F3955AFDC7A74934A611E6C7F218AE)

2. 解压到指定目录，以~/tools为例(~代表根目录，例子中的~等同于/home/XRseries)

   ```
   mkdir -p ~/tools
   tar -jxvf gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2 -C ~/tools
   ```


3. 修改device/soc/AWOL/xradio/xr806/build/Makefile，修改CC_TOOL。

   ```
   CC_TOOL := /home/XRseries/tools/gcc-arm-none-eabi-10-2020-q4-major/bin/arm-none-eabi-
   ```

4. 修改对应的device/board/AWOL/xradio/liteos_m/config.gni，修改board_toolchain_prefix。

   ```
   board_toolchain_prefix = "/home/XRseries/tools/gcc-arm-none-eabi-10-2020-q4-major/bin/arm-none-eabi-"
   #注意！！！config.gni中的board_toolchain_prefix不能使用"~"符号代替根目录
   #否则build_ext_component等命令调用board_toolchain_prefix时会解析错误
   ```

## 编译流程

1. 编译构建说明请[点击跳转](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/subsystems/subsys-build-mini-lite.md)

2. 首次编译前，如果之前已经安装过旧版的hb，会导致编译异常，请重新安装hb。

   ```
   pip3 uninstall ohos-build
   pip3 install build/lite
   ```
   
2. 以xradio_wifi_demo的编译说明为例

   ```
   hb set -root .
   
   AWOL
    > xradio_wifi_demo
   
   选择xradio_wifi_demo
   
   hb build -f
   ```

## 烧录打印

编译生成的固件保存在out/xradio/xradio_wifi_demo目录下，

烧录软件为device/soc/AWOL/xradio/xr806/tools：phoenixMC_v3.1.21014b.exe

### 烧录软件界面

![主页面](./docs/img/image-flash-mainView.png)

### 操作步骤

1. PC安装CP2102驱动。([点击下载](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers))
2. 串口连接：通过USB-typeC数据线连接开发板和PC。
3. 串口设置：点击左上角的“刷新”按钮可刷新已连接串口设备列表，勾选开发板对应的COM口。串口波特率最大支持3000000，波特率越高，烧录速度越快。如果高波特率下容易出现烧录失败，可检查串口线、串口驱动是否稳定支持该波特率；或者降低波特率进行尝试。为了避免烧录速度过慢，建议波特率选择大于等于921600。
4. 固件选择：点击“选择固件”按钮选择需要烧录的固件文件（xr_system.img），固件信息栏会显示出当前固件的详细信息。另外，通过拖拽方式将固件直接拖入工具界面也可以达到同样的效果。
5. 开启一键烧录功能：点击“设置”按钮调出设置界面，如下图勾选“硬件复位烧写模式”

![烧录设置](./docs/img/image-flash-setView.png)

6. 启动烧录：点击“升级固件”按钮启动固件烧录。烧录状态栏显示当前选定串口对应设备的烧录进度和状态。当烧录成功时，进度条会达到100%的进度并显示为绿色；当烧录失败时，进度条显示为红色并报告错误。

7. 复位设备：固件烧录成功后，开启PC串口调试工具（115200，N，8，1），硬件复位开发板（按下复位按钮），将看到以下打印输出。

![开机页面](./docs/img/image-flash-log.png)

## 更多资料

SDK使用方法，调试手段请参考[docs](./docs)，内容持续更新。

