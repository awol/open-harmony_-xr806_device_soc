/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __XRGPIO_H__
#define __XRGPIO_H__

#include "gpio_core.h"
#include "gpio_if.h"
#include "driver/chip/hal_gpio.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    GPIOA_PIN0 = 0,
    GPIOA_PIN1 = 1,
    GPIOA_PIN2 = 2,
    GPIOA_PIN3 = 3,
    GPIOA_PIN4 = 4,
    GPIOA_PIN5 = 5,
    GPIOA_PIN6 = 6,
    GPIOA_PIN7 = 7,
    GPIOA_PIN8 = 8,
    GPIOA_PIN9 = 9,
    GPIOA_PIN10 = 10,
    GPIOA_PIN11 = 11,
    GPIOA_PIN12 = 12,
    GPIOA_PIN13 = 13,
    GPIOA_PIN14 = 14,
    GPIOA_PIN15 = 15,
    GPIOA_PIN16 = 16,
    GPIOA_PIN17 = 17,
    GPIOA_PIN18 = 18,
    GPIOA_PIN19 = 19,
    GPIOA_PIN20 = 20,
    GPIOA_PIN21 = 21,
    GPIOA_PIN22 = 22,
    GPIOA_PIN23 = 23,
    GPIOA_PIN_MAX = GPIOA_PIN23,

    GPIOB_PIN0 = 24,
    GPIOB_PIN1 = 25,
    GPIOB_PIN2 = 26,
    GPIOB_PIN3 = 27,
    GPIOB_PIN4 = 28,
    GPIOB_PIN5 = 29,
    GPIOB_PIN6 = 30,
    GPIOB_PIN7 = 31,
    GPIOB_PIN8 = 32,
    GPIOB_PIN9 = 33,
    GPIOB_PIN10 = 34,
    GPIOB_PIN11 = 35,
    GPIOB_PIN12 = 36,
    GPIOB_PIN13 = 37,
    GPIOB_PIN14 = 38,
    GPIOB_PIN15 = 39,
    GPIOB_PIN16 = 40,
    GPIOB_PIN17 = 41,
    GPIOB_PIN18 = 42,
    GPIO_PIN_MAX
} IotGpioID;

#ifdef __cplusplus
}
#endif

#endif /*__XRGPIO_H__*/