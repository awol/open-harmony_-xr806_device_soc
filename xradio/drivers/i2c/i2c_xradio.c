/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdlib.h>
#include <securec.h>
#include "i2c_core.h"
#include "i2c_if.h"
#include "device_resource_if.h"
#include "driver/chip/hal_i2c.h"

static int32_t i2cHostTransfer(struct I2cCntlr *cntlr, struct I2cMsg *msgs,
                    int16_t count)
{
    int32_t ret = 0;
    struct I2cDevice *device = NULL;
    if (cntlr == NULL || msgs == NULL) {
        HDF_LOGD("%s: I2cCntlr or msgs is NULL\r\n", __func__);
        return HDF_FAILURE;
    }

    if (msgs->flags == I2C_FLAG_READ) {
        ret = HAL_I2C_Master_Receive_IT(cntlr->busId, msgs->addr,
                        msgs->buf, count);
    } else {
        ret = HAL_I2C_Master_Transmit_IT(cntlr->busId, msgs->addr,
                         msgs->buf, count);
    }

    return ret;
}

struct I2cMethod g_i2cHostMethod = {
    .transfer = i2cHostTransfer,
};

static int32_t AttachI2cDevice(struct I2cCntlr *host,
                   struct HdfDeviceObject *device)
{
    int32_t ret;
    I2C_InitParam initParam;

    if (device->property == NULL) {
        HDF_LOGD("%s: I2C property is NULL\n", __func__);
        return HDF_FAILURE;
    }

    struct DeviceResourceIface *dri =
        DeviceResourceGetIfaceInstance(HDF_CONFIG_SOURCE);
    if (dri == NULL || dri->GetUint32 == NULL) {
        HDF_LOGD("DeviceResourceIface is invalid\n");
        return HDF_FAILURE;
    }
    dri->GetUint32(device->property, "IIC_ID", &host->busId, 0);
    HDF_LOGD("IIC_ID = %d\n", host->busId);
    dri->GetUint32(device->property, "addrMode", &initParam.addrMode, 0);
    HDF_LOGD("addrMode = %d\n", initParam.addrMode);
    dri->GetUint32(device->property, "clockFreq", &initParam.clockFreq, 0);
    HDF_LOGD("clockFreq = %d\n", initParam.clockFreq);

    return (int32_t)HAL_I2C_Init(host->busId, &initParam);
}

static void i2cDriverRelease(struct HdfDeviceObject *device)
{
    struct I2cCntlr *i2cCntrl = NULL;
    int32_t IIC_ID;

    if (device == NULL) {
        HDF_LOGD("%s: device is NULL\r\n", __func__);
        return;
    }
    i2cCntrl = (struct I2cCntlr *)device->priv;
    if (i2cCntrl == NULL) {
        HDF_LOGD("%s: i2cCntrl is NULL\r\n", __func__);
        return;
    }
    i2cCntrl->ops = NULL;

    if (HAL_I2C_DeInit(i2cCntrl->busId) != HAL_OK) {
        return HDF_ERR_INVALID_PARAM;
    }
    I2cCntlrRemove(i2cCntrl);
    free(i2cCntrl);

    return HDF_SUCCESS;
}

static int32_t i2cDriverBind(struct HdfDeviceObject *device)
{
    if (device == NULL) {
        HDF_LOGD("%s: I2c device object is NULL\r\n", __func__);
        return -1;
    }
    return HDF_SUCCESS;
}

static int32_t i2cDriverInit(struct HdfDeviceObject *device)
{
    int32_t ret;
    struct I2cCntlr *host = NULL;

    if (device == NULL) {
        HDF_LOGD("%s: device is NULL\r\n", __func__);
        return HDF_ERR_INVALID_OBJECT;
    }
    host = malloc(sizeof(struct I2cCntlr));
    if (host == NULL) {
        HDF_LOGD("%s: host is NULL\r\n", __func__);
        return HDF_FAILURE;
    }
    (void)memset_s(host, sizeof(struct I2cCntlr), 0,
               sizeof(struct I2cCntlr));

    if (AttachI2cDevice(host, device) != HAL_OK) {
        free(host);
        return HDF_ERR_INVALID_PARAM;
    }

    host->ops = &g_i2cHostMethod;
    device->priv = (void *)host;
    ret = I2cCntlrAdd(host);
    if (ret != HDF_SUCCESS) {
        HDF_LOGD("I2cCntlrAdd failed!!ret = %d\n", ret);
        i2cDriverRelease(device);
        return HDF_FAILURE;
    }
    return ret;
}

struct HdfDriverEntry g_i2cDriverEntry = {
    .moduleVersion = 1,
    .moduleName = "XRADIO_I2C",
    .Bind = i2cDriverBind,
    .Init = i2cDriverInit,
    .Release = i2cDriverRelease,
};

HDF_INIT(g_i2cDriverEntry);
