/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __CMD_HM_H__
#define __CMD_HM_H__

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#define HMCMD_DBG_ON 1
#define HMCMD_WRN_ON 1
#define HMCMD_ERR_ON 1

#define HMCMD_SYSLOG printf

#define HMCMD_LOG(flags, fmt, arg...)                                          \
    do {                                                                   \
        if (flags)                                                     \
            HMCMD_SYSLOG(fmt, ##arg);                              \
    } while (0)

#define HMCMD_DBG(fmt, arg...) HMCMD_LOG(HMCMD_DBG_ON, "[cmd] " fmt, ##arg)

#define HMCMD_WRN(fmt, arg...) HMCMD_LOG(HMCMD_WRN_ON, "[cmd WRN] " fmt, ##arg)

#define HMCMD_ERR(fmt, arg...)                                                 \
    HMCMD_LOG(HMCMD_ERR_ON, "[cmd ERR] %s():%d, " fmt, __func__, __LINE__, ##arg)

#define cmd_nitems(a) (sizeof((a)) / sizeof((a)[0]))

enum cmd_status cmd_hm_exec(char *cmd);
void cmd_hm_print_uint8_array(uint8_t *buf, int32_t size);
#ifdef __cplusplus
}
#endif

#endif
