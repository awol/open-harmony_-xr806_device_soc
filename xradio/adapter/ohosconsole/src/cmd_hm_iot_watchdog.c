/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include "os.h"
#include "common/cmd/cmd.h"
#include "iot_watchdog.h"
#include "cmd_hm.h"

static enum cmd_status cmd_wdg_init_exec(char *cmd)
{
    IoTWatchDogEnable();
    return CMD_STATUS_OK;
}

static enum cmd_status cmd_wdg_kick_exec(char *cmd)
{
    IoTWatchDogKick();
    return CMD_STATUS_OK;
}

static enum cmd_status cmd_wdg_disable_exec(char *cmd)
{
    IoTWatchDogDisable();
    return CMD_STATUS_OK;
}

static const struct cmd_data g_iot_watchdog_cmds[] = {
    { "enable", cmd_wdg_init_exec, "  " },
    { "kick", cmd_wdg_kick_exec, " " },
    { "disable", cmd_wdg_disable_exec, " " },
};

enum cmd_status cmd_hm_iot_watchdog_exec(char *cmd)
{
    return cmd_exec(cmd, g_iot_watchdog_cmds,
            cmd_nitems(g_iot_watchdog_cmds));
}
