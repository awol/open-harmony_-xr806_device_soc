/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include "os.h"
#include "iot_uart.h"
#include "iot_errno.h"
#include "common/cmd/cmd.h"
#include "cmd_hm.h"

static enum cmd_status cmd_uart_init_exec(char *cmd)
{
    uint32_t cnt, id, baudRate, dataBits, stopBits;
    char parity[8];
    IotUartAttribute param;

    cnt = cmd_sscanf(cmd, "id=%d b=%d d=%d s=%d p=%7s", &id, &baudRate,
             &dataBits, &stopBits, parity);
    if (cnt != 5) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }

    if (id >= UART_NUM) {
        HMCMD_ERR("invalid id %u\n", id);
        return CMD_STATUS_INVALID_ARG;
    }

    switch (dataBits) {
    case 5:
        param.dataBits = IOT_UART_DATA_BIT_5;
        break;

    case 6:
        param.dataBits = IOT_UART_DATA_BIT_6;
        break;

    case 7:
        param.dataBits = IOT_UART_DATA_BIT_7;
        break;

    case 8:
        param.dataBits = IOT_UART_DATA_BIT_8;
        break;

    default:
        HMCMD_ERR("param.dataBits para err\n");
        return CMD_STATUS_INVALID_ARG;
        break;
    }

    if (cmd_strcmp(parity, "none") == 0) {
        param.parity = IOT_UART_PARITY_NONE;
    } else if (cmd_strcmp(parity, "odd") == 0) {
        param.parity = IOT_UART_PARITY_ODD;
    } else if (cmd_strcmp(parity, "even") == 0) {
        param.parity = IOT_UART_PARITY_EVEN;
    } else {
        HMCMD_ERR("invalid parity %s\n", parity);
        return CMD_STATUS_INVALID_ARG;
    }

    switch (stopBits) {
    case 1:
        param.stopBits = IOT_UART_STOP_BIT_1;
        break;

    case 2:
        param.stopBits = IOT_UART_STOP_BIT_2;
        break;

    default:
        HMCMD_ERR("param.stopBits para err\n");
        return CMD_STATUS_INVALID_ARG;
        break;
    }
    param.baudRate = baudRate;

    return (IoTUartInit(id, &param) == IOT_SUCCESS) ? CMD_STATUS_OK :
                                CMD_STATUS_FAIL;
}

static enum cmd_status cmd_uart_deinit_exec(char *cmd)
{
    uint32_t cnt, id;
    cnt = cmd_sscanf(cmd, "id=%d", &id);
    if (cnt != 1) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }

    return (IoTUartDeinit(id) == IOT_SUCCESS) ? CMD_STATUS_OK :
                              CMD_STATUS_FAIL;
}

static enum cmd_status cmd_uart_write_exec(char *cmd)
{
    uint32_t cnt, id;
    char buffer[20];
    uint8_t len_str;
    cnt = cmd_sscanf(cmd, "id=%d", &id);
    if (cnt != 1) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }
    len_str = snprintf(buffer, 20, "uart%u poll mode.\n", id);

    return (IoTUartWrite(id, (uint8_t *)buffer, len_str) != IOT_FAILURE) ?
                 CMD_STATUS_OK :
                 CMD_STATUS_FAIL;
}

static enum cmd_status cmd_uart_read_exec(char *cmd)
{
    uint32_t cnt, id, size;
    int res = -1;
    unsigned char *rbuf = NULL;

    cnt = cmd_sscanf(cmd, "id=%d size=%d", &id, &size);
    if (cnt != 2) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }

    if (size == 0) {
        CMD_ERR("size is 0!!\n");
        return CMD_STATUS_INVALID_ARG;
    }

    rbuf = malloc(size);
    if (rbuf == NULL) {
        CMD_ERR("no memory\n");
        return CMD_STATUS_FAIL;
    }
    cnt = 0;
    while (1) {
        res = IoTUartRead(id, rbuf, size);
        if (res >= size) {
            IoTUartWrite(id, (uint8_t *)rbuf, res);
            break;
        }
        if (++cnt >= 10) {
            break;
        }
    }
    free(rbuf);

    return (res != CMD_STATUS_FAIL) ? CMD_STATUS_OK : CMD_STATUS_FAIL;
}

static const struct cmd_data g_iot_uart_cmds[] = {
    { "init", cmd_uart_init_exec, CMD_DESC("uart command") },
    { "deinit", cmd_uart_deinit_exec, CMD_DESC("uart command") },
    { "write", cmd_uart_write_exec, CMD_DESC("uart command") },
    { "read", cmd_uart_read_exec, CMD_DESC("uart command") },
};

enum cmd_status cmd_hm_iot_uart_exec(char *cmd)
{
    return cmd_exec(cmd, g_iot_uart_cmds, cmd_nitems(g_iot_uart_cmds));
}
