/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <string.h>
#include "os.h"
#include "iot_errno.h"
#include "common/cmd/cmd.h"
#include "cmd_hm.h"
#include "iot_i2c.h"

static enum cmd_status cmd_i2c_init_exec(char *cmd)
{
    uint32_t cnt, id, bate;
    cnt = cmd_sscanf(cmd, "id=%d b=%d", &id, &bate);
    if (cnt != 2) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }

    return (IoTI2cInit(id, bate) == IOT_SUCCESS) ? CMD_STATUS_OK :
                                 CMD_STATUS_FAIL;
}

static enum cmd_status cmd_i2c_deinit_exec(char *cmd)
{
    uint32_t cnt, id;
    cnt = cmd_sscanf(cmd, "id=%d", &id);
    if (cnt != 1) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }

    return (IoTI2cDeinit(id) == IOT_SUCCESS) ? CMD_STATUS_OK :
                             CMD_STATUS_FAIL;
}

static enum cmd_status cmd_i2c_write_exec(char *cmd)
{
    uint32_t cnt, id, add, size;
    uint8_t data[6];
    data[0] = 0;
    cnt = cmd_sscanf(cmd, "id=%d add=%d len=%d data=%x %x %x %x %x", &id,
             &add, &size, &data[1], &data[2], &data[3], &data[4],
             &data[5]);
    if ((cnt <= 3) || (cnt >= 9)) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }
    if (size > 5) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }
    size++;
    return (IoTI2cWrite(id, add, &data, size) == IOT_SUCCESS) ?
                 CMD_STATUS_OK :
                 CMD_STATUS_FAIL;
}

// #define GC0308_SCCB_ID			0x21 //GC0308 ID
// #define GC0308_CHIP_ID			0x9b

// static enum cmd_status cmd_i2c_get_0308id_exec(char *cmd)
// {
// 	#if 0
// 	IoTI2cWrite(0, GC0308_SCCB_ID, const unsigned char *data, unsigned int dataLen);

// 	HAL_I2C_SCCB_Master_Transmit_IT(priv->i2c_id, GC0308_SCCB_ID, sub_addr, &data);
// 	GC0308_WriteSccb(0XFE, 0x80);//add data
// 	OS_MSleep(100);
// 	GC0308_WriteSccb(0XFE, 0x00);
// 	OS_MSleep(100);

//     if (GC0308_ReadSccb(0x00, &chip_id) != 1) {
//         GC0308_LOGE("GC0308 sccb read error\n");
//         return HAL_ERROR;
//     } else {
// 	    if(chip_id!= GC0308_CHIP_ID) {
// 		    GC0308_LOGE("GC0308 get chip id wrong 0x%02x\n", chip_id);
// 		    return HAL_ERROR;
// 	    } else {
// 		    GC0308_LOGI("GC0308 chip id read success 0x%02x\n", chip_id);
// 	    }
//     }
// #endif
// 	return 0;
// }

static enum cmd_status cmd_i2c_read_exec(char *cmd)
{
    uint32_t cnt, id, size, add, res;
    unsigned char *rbuf = NULL;
    unsigned char data = 0;

    cnt = cmd_sscanf(cmd, "id=%d add=%d size=%d", &id, &add, &size);
    if (cnt != 3) {
        HMCMD_ERR("err cmd:%s\n", cmd);
        return CMD_STATUS_INVALID_ARG;
    }

    if (size == 0) {
        CMD_ERR("size is 0!!\n");
        return CMD_STATUS_INVALID_ARG;
    }

    rbuf = malloc(size);
    if (rbuf == NULL) {
        CMD_ERR("no memory\n");
        return CMD_STATUS_FAIL;
    }
    IoTI2cWrite(id, add, &data, 1);
    res = IoTI2cRead(id, add, rbuf, size);
    if (res == IOT_SUCCESS) {
        cmd_hm_print_uint8_array(rbuf, size);
    }
    free(rbuf);

    return (res == IOT_SUCCESS) ? CMD_STATUS_OK : CMD_STATUS_FAIL;
}

static const struct cmd_data g_iot_i2c_cmds[] = {
    { "init", cmd_i2c_init_exec, CMD_DESC("i2c command") },
    { "deinit", cmd_i2c_deinit_exec, CMD_DESC("i2c command") },
    { "write", cmd_i2c_write_exec, CMD_DESC("i2c command") },
    { "read", cmd_i2c_read_exec, CMD_DESC("i2c command") },
    //{ "getid", cmd_i2c_get_0308id_exec, CMD_DESC("i2c command") },
};

enum cmd_status cmd_hm_iot_i2c_exec(char *cmd)
{
    return cmd_exec(cmd, g_iot_i2c_cmds, cmd_nitems(g_iot_i2c_cmds));
}
