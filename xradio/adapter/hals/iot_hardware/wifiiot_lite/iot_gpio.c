/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @addtogroup IotHardware
 * @{
 *
 * @brief Provides APIs for operating devices,
 * including flash, GPIO, I2C, PWM, UART, and watchdog APIs.
 *
 *
 *
 * @since 2.2
 * @version 2.2
 */
#include "iot_errno.h"
#include "iot_gpio.h"
#include "driver/chip/hal_gpio.h"

typedef enum {
    GPIOA_PIN0 = 0,
    GPIOA_PIN1 = 1,
    GPIOA_PIN2 = 2,
    GPIOA_PIN3 = 3,
    GPIOA_PIN4 = 4,
    GPIOA_PIN5 = 5,
    GPIOA_PIN6 = 6,
    GPIOA_PIN7 = 7,
    GPIOA_PIN8 = 8,
    GPIOA_PIN9 = 9,
    GPIOA_PIN10 = 10,
    GPIOA_PIN11 = 11,
    GPIOA_PIN12 = 12,
    GPIOA_PIN13 = 13,
    GPIOA_PIN14 = 14,
    GPIOA_PIN15 = 15,
    GPIOA_PIN16 = 16,
    GPIOA_PIN17 = 17,
    GPIOA_PIN18 = 18,
    GPIOA_PIN19 = 19,
    GPIOA_PIN20 = 20,
    GPIOA_PIN21 = 21,
    GPIOA_PIN22 = 22,
    GPIOA_PIN23 = 23,
    GPIOA_PIN_MAX = GPIOA_PIN23,

    GPIOB_PIN0 = 24,
    GPIOB_PIN1 = 25,
    GPIOB_PIN2 = 26,
    GPIOB_PIN3 = 27,
    GPIOB_PIN4 = 28,
    GPIOB_PIN5 = 29,
    GPIOB_PIN6 = 30,
    GPIOB_PIN7 = 31,
    GPIOB_PIN8 = 32,
    GPIOB_PIN9 = 33,
    GPIOB_PIN10 = 34,
    GPIOB_PIN11 = 35,
    GPIOB_PIN12 = 36,
    GPIOB_PIN13 = 37,
    GPIOB_PIN14 = 38,
    GPIOB_PIN15 = 39,
    GPIOB_PIN16 = 40,
    GPIOB_PIN17 = 41,
    GPIOB_PIN18 = 42,
    GPIO_PIN_MAX
} IotGpioID;

/**
 * @brief Initializes a GPIO device.
 *
 * @param id Indicates the GPIO pin number.
 * @return Returns {@link IOT_SUCCESS} if the GPIO device is initialized;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioInit(unsigned int id)
{
    GPIO_InitParam param;
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioInit id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));
    printf("IoTGpioInit port%x, pin%u\r\n", (uint32_t)port,
           (uint32_t)(pin));

    param.driving = GPIO_DRIVING_LEVEL_1;
    param.mode = GPIOx_Pn_F0_INPUT;
    param.pull = GPIO_PULL_NONE;
    HAL_GPIO_Init(port, pin, &param);

    return IOT_SUCCESS;
}

/**
 * @brief Deinitializes a GPIO device.
 *
 * @param id Indicates the GPIO pin number.
 * @return Returns {@link IOT_SUCCESS} if the GPIO device is deinitialized;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioDeinit(unsigned int id)
{
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioDeinit id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));
    HAL_GPIO_DeInit(port, pin);

    return IOT_SUCCESS;
}

/**
 * @brief Sets the direction for a GPIO pin.
 *
 * @param id Indicates the GPIO pin number.
 * @param dir Indicates the GPIO input/output direction.
 * @return Returns {@link IOT_SUCCESS} if the direction is set;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioSetDir(unsigned int id, IotGpioDir dir)
{
    GPIO_WorkMode mode;
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioSetDir id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));
    mode = dir ? GPIOx_Pn_F1_OUTPUT : GPIOx_Pn_F0_INPUT;
    HAL_GPIO_SetMode(port, pin, mode);
    //if (dir == IOT_GPIO_DIR_IN) {
    //  HAL_GPIO_SetPull(port, pin, GPIO_PULL_UP);
    //}

    return IOT_SUCCESS;
}

/**
 * @brief Obtains the direction for a GPIO pin.
 *
 * @param id Indicates the GPIO pin number.
 * @param dir Indicates the pointer to the GPIO input/output direction.
 * @return Returns {@link IOT_SUCCESS} if the direction is obtained;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioGetDir(unsigned int id, IotGpioDir *dir)
{
    GPIO_Port port;
    GPIO_Pin pin;
    GPIO_InitParam param;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioGetDir id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));
    HAL_GPIO_GetConfig(port, pin, &param);
    if (param.mode == GPIOx_Pn_F0_INPUT) {
        *dir = IOT_GPIO_DIR_IN;
    } else if (param.mode == GPIOx_Pn_F1_OUTPUT) {
        *dir = IOT_GPIO_DIR_OUT;
    } else {
        printf("IoTGpioGetDir id=%u dir not found!\r\n", id);
        return IOT_FAILURE;
    }

    return IOT_SUCCESS;
}

/**
 * @brief Sets the output level value for a GPIO pin.
 *
 * @param id Indicates the GPIO pin number.
 * @param val Indicates the output level value.
 * @return Returns {@link IOT_SUCCESS} if the output level value is set;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioSetOutputVal(unsigned int id, IotGpioValue val)
{
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioSetOutputVal id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));
    HAL_GPIO_WritePin(port, pin, val ? GPIO_PIN_HIGH : GPIO_PIN_LOW);

    return IOT_SUCCESS;
}

/**
 * @brief Obtains the output level value of a GPIO pin.
 *
 * @param id Indicates the GPIO pin number.
 * @param val Indicates the pointer to the output level value.
 * @return Returns {@link IOT_SUCCESS} if the output level value is obtained;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioGetOutputVal(unsigned int id, IotGpioValue *val)
{
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioGetOutputVal id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));

    *val = (IotGpioValue)HAL_GPIO_ReadPin(port, pin);

    return IOT_SUCCESS;
}

/**
 * @brief Obtains the input level value of a GPIO pin.
 *
 * @param id Indicates the GPIO pin number.
 * @param val Indicates the pointer to the input level value.
 * @return Returns {@link IOT_SUCCESS} if the input level value is obtained;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioGetInputVal(unsigned int id, IotGpioValue *val)
{
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioGetInputVal id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));

    *val = (IotGpioValue)HAL_GPIO_ReadPin(port, pin);

    return IOT_SUCCESS;
}

/**
 * @brief Enables the interrupt feature for a GPIO pin.
 *
 * This function can be used to set the interrupt type, interrupt polarity, and interrupt callback for a GPIO pin.
 *
 * @param id Indicates the GPIO pin number.
 * @param intType Indicates the interrupt type.
 * @param intPolarity Indicates the interrupt polarity.
 * @param func Indicates the interrupt callback function.
 * @param arg Indicates the pointer to the argument used in the interrupt callback function.
 * @return Returns {@link IOT_SUCCESS} if the interrupt feature is enabled;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioRegisterIsrFunc(unsigned int id, IotGpioIntType intType,
                    IotGpioIntPolarity intPolarity,
                    GpioIsrCallbackFunc func, char *arg)
{
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioRegisterIsrFunc id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));

    GPIO_IrqParam irq_param;
    irq_param.arg = arg;
    irq_param.callback = (GPIO_IRQCallback)func;
    if (intType == IOT_INT_TYPE_LEVEL) {
        irq_param.event = intPolarity ? GPIO_IRQ_EVT_HIGH_LEVEL :
                              GPIO_IRQ_EVT_LOW_LEVEL;
    } else {
        irq_param.event = intPolarity ? GPIO_IRQ_EVT_RISING_EDGE :
                              GPIO_IRQ_EVT_FALLING_EDGE;
    }
    HAL_GPIO_EnableIRQ(port, pin, &irq_param);

    return IOT_SUCCESS;
}

/**
 * @brief Disables the interrupt feature for a GPIO pin.
 *
 * @param id Indicates the GPIO pin number.
 * @return Returns {@link IOT_SUCCESS} if the interrupt feature is disabled;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioUnregisterIsrFunc(unsigned int id)
{
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioUnregisterIsrFunc id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));

    HAL_GPIO_DisableIRQ(port, pin);
    return IOT_SUCCESS;
}

/**
 * @brief Masks the interrupt feature for a GPIO pin.
 *
 * @param id Indicates the GPIO pin number.
 * @param mask Indicates whether the interrupt function is masked.
 * The value <b>1</b> means to mask the interrupt function, and <b>0</b> means not to mask the interrupt function.
 * @return Returns {@link IOT_SUCCESS} if the interrupt feature is masked;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioSetIsrMask(unsigned int id, unsigned char mask)
{
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioSetIsrMask id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));

    HAL_GPIO_MaskIRQ(port, pin, mask);

    return IOT_SUCCESS;
}

/**
 * @brief Sets the interrupt trigger mode of a GPIO pin.
 *
 * This function configures a GPIO pin based on the interrupt type and interrupt polarity.
 *
 * @param id Indicates the GPIO pin number.
 * @param intType Indicates the interrupt type.
 * @param intPolarity Indicates the interrupt polarity.
 * @return Returns {@link IOT_SUCCESS} if the interrupt trigger mode is set;
 * returns {@link IOT_FAILURE} otherwise. For details about other return values, see the chip description.
 * @since 2.2
 * @version 2.2
 */
unsigned int IoTGpioSetIsrMode(unsigned int id, IotGpioIntType intType,
                   IotGpioIntPolarity intPolarity)
{
    GPIO_Port port;
    GPIO_Pin pin;

    if (id >= GPIO_PIN_MAX) {
        printf("IoTGpioSetIsrMode id=%u error\r\n", id);
        return IOT_FAILURE;
    }

    port = (id > GPIOA_PIN_MAX) ? GPIO_PORT_B : GPIO_PORT_A;
    pin = (GPIO_Pin)((id > GPIOA_PIN_MAX) ? (id - GPIOA_PIN_MAX - 1) :
                              (id));

    GPIO_InitParam param;
    param.driving = GPIO_DRIVING_LEVEL_1;
    param.mode = GPIOx_Pn_F6_EINT;
    param.pull = GPIO_PULL_NONE;
    HAL_GPIO_Init(port, pin, &param);

    return IOT_SUCCESS;
}

/** @} */
