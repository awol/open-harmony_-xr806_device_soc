/*
 * Copyright (C) 2021 XRADIO TECHNOLOGY CO., LTD. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the
 *       distribution.
 *    3. Neither the name of XRADIO TECHNOLOGY CO., LTD. nor the names of
 *       its contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef _WIFI_DEVICE_UTIL_H
#define _WIFI_DEVICE_UTIL_H

#include "xr_wifi_adapter.h"

#include "wifi_device.h"
#include "wifi_device_config.h"
#include "wifi_error_code.h"

/**
 * @brief convert os security type to hisi security type
 *
 * @param type [in] os security type.
 *
 * @return hisi security type.
 */
xr_wifi_auth_mode HoSecToHiSec(WifiSecurityType type);

/**
 * @brief convert hisi security type to os security type
 *
 * @param type [in] hisi security type.
 *
 * @return os security type.
 */
WifiSecurityType HiSecToHoSec(xr_wifi_auth_mode mode);

/**
 * @brief get hotspot channel
 *
 * @return current channel.
 */
int GetHotspotChannel(void);

/**
 * @brief get hotspot interface name
 *
 * @param result [out] hotspot interface name.
 * @param size   [in] result buffer size, must bigger than WIFI_IFNAME_MAX_SIZE + 1.
 *
 * @return WifiErrorCode.
 */
WifiErrorCode GetHotspotInterfaceName(char *result, int size);

/**
 * @brief lock wifi global lock
 *
 * @return WifiErrorCode.
 */
WifiErrorCode LockWifiGlobalLock(void);

/**
 * @brief unlock wifi global lock
 *
 * @return WifiErrorCode.
 */
WifiErrorCode UnlockWifiGlobalLock(void);

/**
 * @brief lock wifi event lock
 *
 * @return WifiErrorCode.
 */
WifiErrorCode LockWifiEventLock(void);

/**
 * @brief unlock wifi event lock
 *
 * @return WifiErrorCode.
 */
WifiErrorCode UnlockWifiEventLock(void);

/**
 * @brief convert channel to frequency in 2G
 *
 * @return frequency.
 */
unsigned int ChannelToFrequency(unsigned int channel);

unsigned int FrequencyToChannel(unsigned int frequency);
#endif // _WIFI_DEVICE_UTIL_H
