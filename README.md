# device_allwinner

## 目录介绍

```shell
device/soc/AWOL
├── Kconfig.liteos_m.defconfig				# kconfig 默认宏配置
├── Kconfig.liteos_m.series					# 同系列芯片配置
├── Kconfig.liteos_m.soc					# 芯片SOC配置
├── xradio									# 芯片系列名称
│   ├── <commom>							# XR系列芯片公共代码				
│   ├── xr806								# 芯片SOC名称
│   └── ...									# 芯片SOC名称
│
├── vradio                                  # 芯片系列名称
└── ...										# 芯片系列名称
```

## 环境搭建

请参照[OpenHarmony-QuickStart](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-env-setup.md)。

## 获取源码

1. 安装码云repo工具，可以执行如下命令

```shell
curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > /usr/local/bin/repo  
#如果没有权限，可下载至其他目录，并将其配置到环境变量中
chmod a+x /usr/local/bin/repo
pip3 install -i https://repo.huaweicloud.com/repository/pypi/simple requests
```

2. 设置镜像源（可选）

```shell
vim ~/.bashrc
#在文件的最后输入以下内容
export PATH=~/bin:$PATH
export REPO_URL=https://mirrors.tuna.tsinghua.edu.cn/git/git-repo/
#设置完成后重启shell
#设置为清华镜像源后，下载源码时如果提示server certificate verification failed，输入export GIT_SSL_NO_VERIFY=1后重新下载即可。
```

3. 下载源码

```shell
# OpenHarmony通用于各种系统，导致整个系统文件比较多，XR806把部分不必要的代码仓在xml中删除了。
# 如果想要减少或增加需要下载的代码仓，请把manifest仓fork到本地后，自行裁剪。
repo init -u https://gitee.com/awol/open-harmony_-xr806_manifest.git -b master --no-repo-verify -m devboard_xr806.xml
repo sync -c
repo forall -c 'git lfs pull'
```

## 快速入门
- [xradio](./xradio/README.md)。

## 相关仓

[vendor_allwinner](https://gitee.com/openharmony-sig/vendor_allwinner_xr806)
